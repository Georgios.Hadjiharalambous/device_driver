Development of "Lunix", a Linux-driver for a remote device measuring temperature, light and its battery. <br/>
A project for Laboratory of operating systems course at NTUA ECE. <br/>
Developed by [Myrsini Vakalopoulou ](https://gitlab.com/mdvakalop) and [Georgios Hadjiharalambous](https://gitlab.com/Georgios.Hadjiharalambous)